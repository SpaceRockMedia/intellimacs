" List bookmarks
" Create bookmark 0 with <C-S-0>, Create bookmark 1 with <C-S-1> and so on.
" Go to bookmark 0 with <C-0>, go to bookmark 1 with <C-1> and so on.
let g:WhichKeyDesc_Bookmarks_List = "<leader>Bl list-bookmarks"
nnoremap <leader>Bl    :action ShowBookmarks<CR>
vnoremap <leader>Bl    :action ShowBookmarks<CR>

" Previous bookmark
let g:WhichKeyDesc_Bookmarks_PreviousN = "<leader>BN previous-bookmark"
nnoremap <leader>BN    :action GotoPreviousBookmark<CR>
vnoremap <leader>BN    <Esc>:action GotoPreviousBookmark<CR>

" Next bookmark
let g:WhichKeyDesc_Bookmarks_Next = "<leader>Bn next-bookmark"
nnoremap <leader>Bn    :action GotoNextBookmark<CR>
vnoremap <leader>Bn    <Esc>:action GotoNextBookmark<CR>

" Previous bookmark
let g:WhichKeyDesc_Bookmarks_Previous = "<leader>Bp previous-bookmark"
nnoremap <leader>Bp    :action GotoPreviousBookmark<CR>
vnoremap <leader>Bp    <Esc>:action GotoPreviousBookmark<CR>

" Toggle bookmark. When you activate it, assign to it a value.
let g:WhichKeyDesc_Bookmarks_ToggleMnemonic = "<leader>BT toggle-bookmark-with-mnemonic"
nnoremap <leader>BT    :action ToggleBookmarkWithMnemonic<CR>
vnoremap <leader>BT    <Esc>:action ToggleBookmarkWithMnemonic<CR>

" Toggle bookmark
let g:WhichKeyDesc_Bookmarks_Toggle = "<leader>Bt toggle-bookmark"
nnoremap <leader>Bt    :action ToggleBookmark<CR>
vnoremap <leader>Bt    :action ToggleBookmark<CR>

nmap `0 <Action>(GotoBookmark0)
nmap `1 <Action>(GotoBookmark1)
nmap `2 <Action>(GotoBookmark2)
nmap `3 <Action>(GotoBookmark3)
nmap `4 <Action>(GotoBookmark4)
nmap `5 <Action>(GotoBookmark5)
nmap `6 <Action>(GotoBookmark6)
nmap `7 <Action>(GotoBookmark7)
nmap `8 <Action>(GotoBookmark8)
nmap `9 <Action>(GotoBookmark9)
nmap `a <Action>(GotoBookmarkA)
nmap `b <Action>(GotoBookmarkB)
nmap `c <Action>(GotoBookmarkC)
nmap `d <Action>(GotoBookmarkD)
nmap `e <Action>(GotoBookmarkE)
nmap `f <Action>(GotoBookmarkF)
nmap `g <Action>(GotoBookmarkG)
nmap `h <Action>(GotoBookmarkH)
nmap `i <Action>(GotoBookmarkI)
nmap `j <Action>(GotoBookmarkJ)
nmap `k <Action>(GotoBookmarkK)
nmap `l <Action>(GotoBookmarkL)
nmap `m <Action>(GotoBookmarkM)
nmap `n <Action>(GotoBookmarkN)
nmap `o <Action>(GotoBookmarkO)
nmap `p <Action>(GotoBookmarkP)
nmap `q <Action>(GotoBookmarkQ)
nmap `r <Action>(GotoBookmarkR)
nmap `s <Action>(GotoBookmarkS)
nmap `t <Action>(GotoBookmarkT)
nmap `u <Action>(GotoBookmarkU)
nmap `v <Action>(GotoBookmarkV)
nmap `w <Action>(GotoBookmarkW)
nmap `x <Action>(GotoBookmarkX)
nmap `y <Action>(GotoBookmarkY)
nmap `z <Action>(GotoBookmarkZ)

nmap b0 <Action>(ToggleBookmark0)
nmap b1 <Action>(ToggleBookmark1)
nmap b2 <Action>(ToggleBookmark2)
nmap b3 <Action>(ToggleBookmark3)
nmap b4 <Action>(ToggleBookmark4)
nmap b5 <Action>(ToggleBookmark5)
nmap b6 <Action>(ToggleBookmark6)
nmap b7 <Action>(ToggleBookmark7)
nmap b8 <Action>(ToggleBookmark8)
nmap b9 <Action>(ToggleBookmark9)
nmap ba <Action>(ToggleBookmarkA)
nmap bb <Action>(ToggleBookmarkB)
nmap bc <Action>(ToggleBookmarkC)
nmap bd <Action>(ToggleBookmarkD)
nmap be <Action>(ToggleBookmarkE)
nmap bf <Action>(ToggleBookmarkF)
nmap bg <Action>(ToggleBookmarkG)
nmap bh <Action>(ToggleBookmarkH)
nmap bi <Action>(ToggleBookmarkI)
nmap bj <Action>(ToggleBookmarkJ)
nmap bk <Action>(ToggleBookmarkK)
nmap bl <Action>(ToggleBookmarkL)
nmap bm <Action>(ToggleBookmarkM)
nmap bn <Action>(ToggleBookmarkN)
nmap bo <Action>(ToggleBookmarkO)
nmap bp <Action>(ToggleBookmarkP)
nmap bq <Action>(ToggleBookmarkQ)
nmap br <Action>(ToggleBookmarkR)
nmap bs <Action>(ToggleBookmarkS)
nmap bt <Action>(ToggleBookmarkT)
nmap bu <Action>(ToggleBookmarkU)
nmap bv <Action>(ToggleBookmarkV)
nmap bw <Action>(ToggleBookmarkW)
nmap bx <Action>(ToggleBookmarkX)
nmap by <Action>(ToggleBookmarkY)
nmap bz <Action>(ToggleBookmarkZ)
